<?php
namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\models\EventState;
use BWB\Framework\mvc\DAO;
use PDO;

class DAOEventState extends DAO
{
    public function create($state) {
        $AccountEvent_id = $this->getPdo()->lastInsertId();
        $req = "INSERT INTO EventState SET AccountEvent_id=?, state=?";
        $db = $this->getPdo();
        $stmt = $db->prepare($req);
        return $stmt->execute(array($AccountEvent_id, $state));
    }

    public function delete($id) {
        
    }

    public function getAll() {
        
    }

    public function getAllBy($filter) {
        
    }

    public function retrieve($id) {
        $result = $this->getPdo()->query("SELECT * FROM EventState WHERE id='" . $id."'");
        //* $result->setFetchMode(PDO::FETCH_ASSOC);
        $res = $result->fetch();
       //*  print_r($res);
        return $res;
    }
    //* met a jour la table lors de la modification/validation de l'event par le créateur
    public function update ($array) {
        $state = $array[0];
        $idAccountEvent = $array[1];
        $req = "UPDATE EventState SET state=? WHERE AccountEvent_id = '".$idAccountEvent."'";
        $db = $this->getPdo();
        $stmt = $db->prepare($req);
        return $stmt->execute(array($state));
    }

   
   
}