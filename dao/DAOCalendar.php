<?php

namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use PDO;

class DAOCalendar extends DAO
{

    public function create($array)
    { }

    public function retrieve($id)
    {
        $req = "SELECT Event.id, FirstName, Event.date_init, Event.date_end, Event.name, Event.description, AccountEvent.Account_id_creator, AccountEvent.Account_id_guest, EventState.state, EventType.type FROM Event
        JOIN AccountEvent ON Event.id = AccountEvent.Event_id
        JOIN EventState ON AccountEvent.id = EventState.AccountEvent_id
        JOIN EventType ON Event.EventType_id = EventType.id
        JOIN Account ON Account.id =  AccountEvent.Account_id_creator
        WHERE Event.id = '" . $id . "' ";
        $result = $this->getPdo()->query($req);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $res = $result->fetchAll();
        foreach ($res as $row) {
            $data[] = array(

                "id"           => $row["id"],
                "title"        => $row["name"],
                "start"        => $row["date_init"],
                "end"          => $row["date_end"],
                "description"  => $row["description"],
                "type"         => $row['type'],
                "state"        => $row['state'],
                'creator'      => $row['Account_id_creator'],
                "firstName"    => $row['FirstName']

            );
        }
        return $data;
    }

    public function update($array)
    { }

    public function delete($id)
    { }

    public function getAll()
    { }

    public function getAllBy($filter)
    { }

    //* récupère les modules de la formation
    public function getModules($id)
    {
        // $result = $this->getPdo()->query("SELECT * FROM EventType WHERE id='" . $id."'");
        // $result->setFetchMode(PDO::FETCH_ASSOC);
        // $res = $result->fetch();
        $req = "SELECT DISTINCT Event.id, Event.date_init, Event.date_end, Event.name, Event.description, AccountEvent.Account_id_creator,  EventState.state, EventType.type FROM Event
        JOIN AccountEvent ON Event.id = AccountEvent.Event_id
        JOIN EventState ON AccountEvent.id = EventState.AccountEvent_id
        JOIN EventType ON Event.EventType_id = EventType.id
        WHERE EventType.type = 'module' AND (EventState.state = 'yes+' OR EventState.state = 'yes'  OR EventState.state = 'maybe') AND( AccountEvent.Account_id_guest = '" . $id . "' OR AccountEvent.Account_id_creator = '" . $id . "')";
        $result = $this->getPdo()->query($req);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $res = $result->fetchAll();
        if (!empty($res)) {
            foreach ($res as $row) {

                $data[] = array(

                    "id"           => $row["id"],
                    "title"        => $row["name"],
                    "start"        => $row["date_init"],
                    "end"          => $row["date_end"],
                    "description"  => $row["description"],
                    "type"         => $row['type'],
                    "state"        => $row['state'],
                    'creator'      => $row['Account_id_creator']
                );
            }
            return $data;
        }
    }

    public function getAutres($id)
    {

        $req = "SELECT  DISTINCT Event.id, Event.date_init, Event.date_end, Event.name, Event.description, AccountEvent.Account_id_creator,  EventState.state, EventType.type FROM Event
        JOIN AccountEvent ON Event.id = AccountEvent.Event_id
        JOIN EventState ON AccountEvent.id = EventState.AccountEvent_id
        JOIN EventType ON Event.EventType_id = EventType.id
        WHERE EventType.type = 'autre' AND (EventState.state = 'yes+' OR EventState.state = 'yes'  OR EventState.state = 'maybe') AND( AccountEvent.Account_id_guest = '" . $id . "' OR AccountEvent.Account_id_creator = '" . $id . "')";
        $result = $this->getPdo()->query($req);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $res = $result->fetchAll();
        if (!empty($res)) {
            foreach ($res as $row) {

                $data[] = array(

                    "id"           => $row["id"],
                    "title"        => $row["name"],
                    "start"        => $row["date_init"],
                    "end"          => $row["date_end"],
                    "description"  => $row["description"],
                    "type"         => $row['type'],
                    "state"        => $row['state'],
                    'creator'      => $row['Account_id_creator']
                );
            }
            return $data;
        }
    }

    public function getRdv($id)
    {
        $req = "SELECT DISTINCT Event.id, Event.date_init, Event.date_end, Event.name, Event.description, AccountEvent.Account_id_creator,  EventState.state, EventType.type FROM Event
        JOIN AccountEvent ON Event.id = AccountEvent.Event_id
        JOIN EventState ON AccountEvent.id = EventState.AccountEvent_id 
        JOIN EventType ON Event.EventType_id = EventType.id
        WHERE EventType.type = 'rdv' AND (EventState.state = 'yes+' OR EventState.state = 'yes'  OR EventState.state = 'maybe') AND( AccountEvent.Account_id_guest = '" . $id . "' OR AccountEvent.Account_id_creator = '" . $id . "')";
        $result = $this->getPdo()->query($req);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $res = $result->fetchAll();
        if (!empty($res)) {
            foreach ($res as $row) {

                $data[] = array(

                    "id"           => $row["id"],
                    "title"        => $row["name"],
                    "start"        => $row["date_init"],
                    "end"          => $row["date_end"],
                    "description"  => $row["description"],
                    "type"         => $row['type'],
                    "state"        => $row['state'],
                    'creator'      => $row['Account_id_creator']
                );
            }
            return $data;
        }
    }
    public function getGuests($id_event)
    {
        $req = "SELECT DISTINCT AccountEvent.Account_id_guest, Account.FirstName, EventState.state FROM AccountEvent, Account, EventState  WHERE 
       AccountEvent.Account_id_guest = Account.id AND  AccountEvent.Event_id = '" . $id_event . "' AND AccountEvent.id = EventState.AccountEvent_id ";
        $result = $this->getPdo()->query($req);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }
}
