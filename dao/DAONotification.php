<?php

namespace BWB\Framework\mvc\dao;

use PDO;
use BWB\Framework\mvc\DAO;

class DAONotification extends DAO
{

    //* METHODS

  
    //* getUnReadMessages($id) : récupère en bdd les messages non lus pour l'utilisateur défini par $id (id Account)
    public function getUnReadMessages($id)
    {
        //*var_dump($id);
        $result = $this->getPdo()->query("SELECT * from  Message INNER JOIN Communication ON Message.id = Communication.Message_id AND Communication.Account_id_receiver= '" . $id . "' AND Message.removed IS NULL INNER JOIN MessageState ON MessageState.readed IS NULL AND Communication.id = MessageState.Communication_id");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rows = [];
        while ($row = $result->fetch()) {
            array_push($rows, $row);
        }
        //var_dump($rows);
        return $rows; //* retourne tous les messages non lus et non supprimés !!
    }

    //* récupère en bdd les events non validé pour l'utilisateur défini par $id (id invité) et récupère en bdd les events qui ont étés validés par un invité pour un utilisateur (id creator)
    public function getNewEvents($id)
    {
        //*var_dump($id);
        $result = $this->getPdo()->query("SELECT Event.id, Event.date_init, Event.name, EventState.AccountEvent_id, EventState.state, AccountEvent.Account_id_guest  from Event,  AccountEvent, EventState WHERE Event.id = AccountEvent.Event_id AND ((AccountEvent.id = EventState.AccountEvent_id AND (EventState.state ='". 'maybe'."' OR EventState.state ='". 'nosuppr'."') AND AccountEvent.Account_id_guest = '".$id."') OR (AccountEvent.id = EventState.AccountEvent_id AND (EventState.state = '".'yes'."' OR EventState.state = '". 'no' ."') AND AccountEvent.Account_id_creator = '".$id."'))");
       
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $rows = [];
        while ($row = $result->fetch()) {
            array_push($rows, $row);
        }
        //*var_dump($rows);
        return $rows; //* retourne tous les events non validés auxquels un utilisateur est invité !!
    }

    

    public function create($array)
    { }

    public function delete($id)
    { }

    public function getAll()
    { }

    public function getAllBy($filter)
    { }

    public function retrieve($id)
    { }

    public function update($array)
    { }
}
