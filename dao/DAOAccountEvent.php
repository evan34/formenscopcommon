<?php
namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\models\AccountEvent;
use BWB\Framework\mvc\DAO;
use PDO;

class DAOAccountEvent extends DAO
{
    public function create($array) {
       // $Eventid = $this->getPdo()->lastInsertId();
        $req = "INSERT INTO AccountEvent SET Event_id=?, Account_id_creator=?, Account_id_guest=?";
        $db = $this->getPdo();
        $stmt = $db->prepare($req);
        return $stmt->execute(array($array['Eventid'], $array['Account_id_creator'], $array['Account_id_guest']));
    }

    public function delete($id) {
        
    }

    public function getAll() {
        
    }

    public function getAllBy($filter) {
        
    }

    public function retrieve($id) {
        $result = $this->getPdo()->query("SELECT * FROM AccountEvent WHERE id='" . $id."'");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $res = $result->fetch();
       //*  print_r($res);
        return $res;
    }

    public function update($array) {
        
    }

   public function getId($eventid, $idguest)
   {
       $result = $this->getPdo()->query("SELECT id FROM AccountEvent WHERE Event_id = '".$eventid."' AND Account_id_guest = '".$idguest."'");
       $result->setFetchMode(PDO::FETCH_ASSOC);
       $res = $result->fetch();
       return $res;
   }
}