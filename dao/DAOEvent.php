<?php
namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\models\Event;
use BWB\Framework\mvc\DAO;
use PDO;

class DAOEvent extends DAO
{
    public function create($array) {
        $req = "INSERT INTO Event SET date_init=?, date_end=?, name=?, description=?, EventType_id=?";
        $db = $this->getPdo();
        $stmt = $db->prepare($req);
        return $stmt->execute(array($array['start'], $array['end'], $array['name'], $array['description'], $array['type_id']));
    }
    public function update($array) {
        $req = "UPDATE Event SET date_init=?, date_end=?, name=?, description=?, EventType_id=? WHERE id= '" . $array['id'] . "'";
        $db = $this->getPdo();
        $stmt = $db->prepare($req);
        return $stmt->execute(array($array['start'], $array['end'], $array['name'], $array['description'], $array['type_id']));
    }
    
    public function delete($id) {
        
    }

    public function getAll()
    {
        $data = array();
        $query = "SELECT * FROM Event ORDER BY id";
        $statment = $this->getPdo()->prepare($query);
        $statment->execute();
        $result = $statment->fetchAll();
        foreach ($result as $row) {
            $data[] = array(
                "id"           => $row["id"],
                "title"        => $row["name"],
                "start"        => $row["date_init"],
                "end"          => $row["date_end"]
            );
        }

        return $data;
    }

    public function getAllBy($filter) {
        
    }

    public function retrieve($id) {
        $result = $this->getPdo()->query("SELECT * FROM Event WHERE id='" . $id."'");
        //* $result->setFetchMode(PDO::FETCH_ASSOC);
        $res = $result->fetch();
       //*  print_r($res);
        return $res;
    }

    
    public function getLastId()
    {
        return $this->getPdo()->lastInsertId();
    }
}