<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOMessage;

class Message extends DefaultModel
{
    protected $id;
    protected $subject;
    protected $texte;
    protected $removed;
    protected $date;

    public function __construct($id = null)
    {
        if(!is_null($id)){
        $this->parse((new DAOMessage())->retrieve($id));}
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setTexte($texte)
    {
        $this->texte = $texte;
    }

    public function getTexte()
    {
        return $this->texte;
    }

    public function setRemoved($removed)
    {
        $this->removed = $removed;
    }

    public function getRemoved()
    {
        return $this->removed;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
    $this->date = $date;
    }
}