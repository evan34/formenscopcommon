<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOEventState;
use BWB\Framework\mvc\models\AccountEvent;

class EventState extends DefaultModel
{
    protected $AccountEvent_id;
    protected $state;

    public function __construct($id = null)
    {
        if(!is_null($id)){

            $this->parse((new DAOEventState())->retrieve($id));
        }
    }

    public function setAccountEvent_id($AccountEvent_id)
    {
        $this->AccountEvent_id = new AccountEvent($AccountEvent_id);
    }

    public function getAccountEvent_id()
    {
        return $this->AccountEvent_id;
    }

    public function setState($state)
    {
        $this->state = $state;
    }

    public function getState()
    {
        return $this->state;
    }

}