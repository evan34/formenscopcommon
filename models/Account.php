<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\models\Address;
use BWB\Framework\mvc\models\Role;
use BWB\Framework\mvc\dao\DAOAccount;

class Account extends DefaultModel
{
    protected $id;
    protected $Name;
    protected $FirstName;
    protected $Birthday;
    protected $Email;
    protected $Password;
    protected $Address_id;
    protected $Role_id;


    public function __construct($id = null)
    {
        if (!is_null($id)) {
            $this->parse((new DAOAccount())->retrieve($id));
        }
    }



    /** GETTERS */


    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->Name;
    }

    public  function getFirstName()
    {
        return $this->FirstName;
    }

    public function getBirthday()
    {
        return $this->Birthday;
    }

  
    public  function getPassword()
    {
        return $this->Password;
    }

    public function getEmail()
    {
        return $this->Email;
    }

    public function getAddress_Id()
    {
        return $this->Address_id;
    }


    public function getRole_id()
    {
        return $this->Role_id;
    }

    public function getRoles()
    {
        return $this->Role_id;
    }

    public function getUsername()
    {
        return $this->Email;
    }


    /** SETTERS */


    public  function setName($Name)
    {
        $this->Name = $Name;
    }

    public  function setFirstName($FirstName)
    {
        $this->FirstName = $FirstName;
    }

    public function setBirthday($Birthday)
    {
        $this->Birthday = $Birthday;
    }

    public function setEmail($Email)
    {
        $this->Email = $Email;
    }

    public  function setPassword($Password)
    {
        $this->Password = $Password;
    }


    public function setAddressId($address_id)
    {
        $this->address_id = new Address($address_id);
    }


    public function setRole_id($role_id)
    {
        return $this->role_id = new Role($role_id);
    }
}
