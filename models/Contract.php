<?php
namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOContract;

class Contract extends DefaultModel
{
    //* Propriétés
    protected $id;
    protected $type;


    //* Constructeur
    public function __construct($id)
    {
        if(!is_null($id)){

            $this->parse((new DAOContract)->retrieve($id));
        }
    }

    
    //* Getters
    public function getId()
    {
        return $this->id;
    }

    public function getType()
    {
        return $this->type;
    }


    //* Setters
    public function setId($id)
    {
       $this->id = $id;
    }

    public function setType($type)
    {
        $this->type = $type;
    }
}