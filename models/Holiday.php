<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\models\Salaried;
use BWB\Framework\mvc\dao\DAOHoliday;

class Holiday extends DefaultModel
{
    protected $id;
    protected $start;
    protected $end;
    protected $status;
    protected $Salaried_Account_id;

    public function __construct($id = null)
    {
        if(!is_null($id)){
        $this->parse((new DAOHoliday())->retrieve($id));}
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setStart($start)
    {
        $this->start = $start;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function setEnd($end)
    {
        $this->end = $end;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setSalaried_Account_id($Salaried_Account_id)
    {
        $this->Salaried_Account_id = new Salaried($Salaried_Account_id);
    }

    public function getSalaried_Account_id()
    {
        return $this->Salaried_Account_id;
    }
}