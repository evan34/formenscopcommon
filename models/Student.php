<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\models\Training;
use BWB\Framework\mvc\models\Trainee;
use BWB\Framework\mvc\dao\DAOStudent;

class Student extends DefaultModel
{
    protected $Trainee_Account_id;
    protected $Training_id;

    public function __construct($id = null)
    {
        if(!is_null($id)){
        $this->parse((new DAOStudent())->retrieve($id));
        }
    }

    public function setTrainee_Account_id($Trainee_Account_id)
    {
        $this->Trainee_Account_id = new Trainee($Trainee_Account_id);
    }

    public function getTrainee_Account_id()
    {
        return $this->Trainee_Account_id;
    }

    public function setTraining_id($Training_id)
    {
        $this->Training_id = new Training($Training_id);
    }

    public function getTraining_id()
    {
        return $this->Training_id;
    }
}