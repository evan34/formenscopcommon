<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\dao\DAOAddress;
use BWB\Framework\mvc\models\DefaultModel;
class Address extends DefaultModel
{
    protected $id;
    protected $zipcode;
    protected $city;
    protected $country;
    protected $number;
    protected $name;
    protected $Type_id;



    public function __construct($id)
    {
        if (!is_null($id)) {
            $this->parse((new DAOAddress())->retrieve($id));
        }
    }

 


    /** GETTERS */

    function getId()
    {
        return $this->id;
    }

    function getZipcode()
    {
        return $this->zipcode;
    }
    function getCity()
    {
        return $this->city;
    }
    function getCountry()
    {
        return $this->country;
    }

    function getNumber()
    {
        return $this->number;
    }
    function getName()
    {
        return $this->name;
    }
    function getType_id()
    {
        return $this->Type_id;
    }


    /** SETTERS */

    function setId($id)
    {
        $this->id = $id;
    }
    function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }
    function setCity($city)
    {
        $this->city = $city;
    }
    function setCountry($country)
    {
        $this->country = $country;
    }

    function setNumber($number)
    {
        $this->number = $number;
    }
    function setName($name)
    {
        $this->name = $name;
    }
    function setType_id($Type_id)
    {
        $this->Type_id = $Type_id;
    }
}
