<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\ModelInterface;

/**
 * classe abstraite implantant la méthode commune à toutes les entities, l'hydratation
 */
abstract class AbstractModel implements ModelInterface
{

    private function __construct($dataArray){
        $this->hydrateModel($dataArray);
    }

    private function hydrateModel($dataArray)
    {
        // On va parcourir le tableau récupèré pour hydrater
        foreach ($dataArray as $key => $value) {
            // On récupère le nom du setter correspondant à $key
            $method = 'set' . ucfirst($key); // ucfirst()=> 1ere lettre en majuscule

            // On vérifie si le setter existe
            if (method_exists($this, $method)){
                // On appelle le setter
                $this->$method($value);
            }
        }
    }
}
