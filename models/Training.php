<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOTraining;

class Training extends DefaultModel
{
    protected $id;
    protected $name;
    protected $start;
    protected $volume;

    public function __construct($id = null)
    {
        if(!is_null($id)){
        $this->parse((new DAOTraining())->retrieve($id));
        }
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->id;
    }

    // public function setEnd($_end)
    // {
    //     $this->_end = $_end;
    // }

    // public function getEnd()
    // {
    //     return $this->_end;
    // }

    public function setVolume($volume)
    {
        $this->volume = $volume;
    }

    public function getVolume()
    {
        return $this->volume;
    }
}