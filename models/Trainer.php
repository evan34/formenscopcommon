<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\models\Training;
use BWB\Framework\mvc\models\Coordinator;
use BWB\Framework\mvc\models\Salaried;
use BWB\Framework\mvc\dao\DAOTrainer;

class Trainer extends DefaultModel
{
    protected $Salaried_Account_id;
    protected $Coordinator_Salaried_Account_id;
    protected $Training_id;

    public function __construct($id = null)
    {
        if(!is_null($id)){
        $this->parse((new DAOTrainer())->retrieve($id));
        }
    }

    public function setSalaried_Account_id($Salaried_Account_id)
    {
        $this->Salaried_Account_id = new Salaried($Salaried_Account_id);
    }

    public function getSalaried_Account_id()
    {
        return $this->Salaried_Account_id;
    }

    public function setCoordinator_Salaried_Account_id($Coordinator_Salaried_Account_id)
    {
        $this->Coordinator_Salaried_Account_id = new Coordinator($Coordinator_Salaried_Account_id);
    }

    public function getCoordinator_Salaried_Account_id()
    {
        return $this->Coordinator_Salaried_Account_id;
    }

    public function setTraining_id($Training_id)
    {
        $this->Training_id = new Training($Training_id);
    }

    public function getTraining_id()
    {
        return $this->Training_id;
    }
}