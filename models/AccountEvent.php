<?php
namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOAccountEvent;
use BWB\Framework\mvc\models\Account;
use BWB\Framework\mvc\models\Event;

class AccountEvent extends DefaultModel
{
    //* Propriétés
    protected $id;
    protected $Event_id;
    protected $Account_id_creator;
    protected $Account_id_guest;


    //* Constructeur
    public function __construct($id)
    {
        if(!is_null($id)){

            $this->parse((new DAOAccountEvent)->retrieve($id));
        }
    }


    //* Getters
    public function getId()
    {
       return $this->id;
    }

    public function getEvent_id()
    {
        return $this->Event_id;
    }

    public function getAccount_id_creator()
    {
        return $this->Account_id_creator;
    }

    public function Account_id_guest()
    {
        return $this->Account_id_guest;
    }


    //* Setters

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setEvent_id($Event_id)
    {
        $this->Event_id = new Event($Event_id);
    }

    public function setAccount_id_creator($id_creator)
    {
        $this->Account_id_creator = new Account($id_creator);
    }

    public function setAccount_id_guest($id_guest)
    {
        $this->Account_id_guest = new Account($id_guest);
    }
}