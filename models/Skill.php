<?php

namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOSkill;


class Skill extends DefaultModel
{
    //* Propriétés (cf bdd)
    protected $id;
    protected $name;
    protected $description;


    //* Constructeur
    public function __construct($id=null){
        if(!isNull($id)){
            $this->parse((new DAOSkill)->retrieve($id));
        }
    }

    // //* Méthode parse()
    // public function parse($array)
    // {
    //     $this->id = $array['id'];
    //     $this->name = $array['name'];
    //     $this->description = $array['description'];
    // }

    //* Getters

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }


    //* Setters

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }
}