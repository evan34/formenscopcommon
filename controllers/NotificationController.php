<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;
use BWB\Framework\mvc\dao\DAONotification;
use function GuzzleHttp\json_encode;

class NotificationController extends Controller
{
    //* PARTIE EVENT

   
    //* récupère les nouveaux events => getNewEvents, prend en paramètre l'id de l'utilisateur
    public function getNewEvent($id)
    {
        $newEvent = (new DAONotification())->getNewEvents($id);
     
        if (empty($newEvent)) {
            
            echo json_encode('NOnews');
        } else {
            $data = array('newEvent' => $newEvent);
            echo json_encode($data);
        }
    }
    //* PARTIE MESSAGE

    //* verifie si nouveau message
    public function getMessageNotification($id)
    {
       
        //header('Content-type: application/json');
        $dao = new DAONotification;
//print_r($dao->getUnReadMessages($id));
        if(empty($dao->getUnReadMessages($id))){
            echo 'false';
        }else{
            echo 'true';
        }
        
    }

    //* récupère les nouveaux messages
    public function getNewMessage($id)
    {
       
        $dao = new DAONotification;
        //var_dump($dao->getUnReadMessages($id));
        echo json_encode($dao->getUnReadMessages($id));
    }
}
