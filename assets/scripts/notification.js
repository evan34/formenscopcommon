/**Quand la page est chargée, récupère l'id de l'utilisateur stocké dans header.php (ligne 20)*/
$().ready(function () {
    $('.idHide').hide();
    let id = $('.idHide').text();

    /**  Vérification si nouveaux events*/
    //* requête vers l'api commune pour vérifier si il y a de nouveaux events
    $.ajax({
        method: 'GET',
        url: `http://formenscop.bwb/event/checknew/${id}`,
        dataType: "JSON",
        success: function (result) {
            if (result === false) {
                //* si il n'y a pas de nouvel event, desactive le bouton event et enlève "le point rouge"
                $('#button_event').attr("data-toggle", "");
                $('#event').attr("class", "");
            } else {
                //* sinon il y a de nouveaux events, requête vers l'api commune pour les récuperer au clic sur l'icone "event"
                $('#button_event').click(function () {
                    $.ajax({
                        method: 'GET',
                        url: `http://formenscop.bwb/event/viewnew/${id}`,
                        dataType: 'JSON',
                        success: function (result) {
                            //* le retour de l'api contient 2 sortes d'event, les event en attente et ceux qui ont étés validés
                            $('#newevent').html('new event');
                            let unValildateEvent = result.unValildateEvent;
                            let validateEvent = result.validateEvent;

                            //* Pour chaque type d'events, affichage dans la liste déroulante
                            for (event of unValildateEvent) {
                                if (event.Account_id_creator !== event.Account_id_guest) {
                                    $('#newevent').append(`<div id="unvalidateevent" <li><h3>${event.name}<span >${event.date_init}</span></h3></li></div>`)
                                }
                            }
                            for (event of validateEvent) {
                                $('#newevent').append(`<div id="validateevent" <li><h3>${event.name} : évènement validé<span >${event.date_init}</span></h3></li></div>`)
                            }
                        }
                    })
                })
            }

        },
    });
    /**  vérification si nouveau messages */
    //* requête vers l'api commune pour vérifier si il y a de nouveaux messages
    $.ajax({
        method: 'GET',
        url: `http://formenscop.bwb/message/checknew/${id}`,
        dataType: "JSON",
        success: function (result) {
            //* si il n'y a pas de nouveau messages, desactive le bouton message et enlève "le point rouge"
            if (result === 'false') {
                $('#button_message').attr("data-toggle", "");
                $('#message').attr("class", "");
                //* sinon il y a de nouveaux messages, requête vers l'api commune pour les récuperer au clic sur l'icone "message"
            } else {
                $('#button_message').click(function () {
                    $.ajax({
                        method: 'GET',
                        url: `http://formenscop.bwb/message/viewnew/${id}`,
                        dataType: "JSON",
                        success: function (result) {
                            //* pour chaque message non lu, on l'affiche dans la liste déroulante
                            $('#newmsg').html('new msg');
                            for (msg of result) {
                                $('#newmsg').append(`<li><h3>${msg.subject}<span >${msg.date}</span></h3></li>`)

                            }
                        }
                    })
                })
            }

        },

    });


});