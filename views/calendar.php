<!DOCTYPE html>
<html>

<head>
	<?php include('include/head.php'); ?>
	<link rel="stylesheet" type="text/css" href="assets/plugins/fullcalendar/fullcalendar.css">
</head>

<body>
	<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>

	<div class="main-container">

		<div id="message">
		</div>
		<div class="pd-ltr-20 xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="title">
								<h4>Calendar</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">

									<li class="breadcrumb-item"><a href="index.php">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Calendar</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<!--Fenetre Pop-up pour CREER UN EVENEMENT-->
				<!--Fenetre Pop-up pour CREER UN EVENEMENT-->
				<!--Fenetre Pop-up pour CREER UN EVENEMENT-->
				<!--Fenetre Pop-up pour CREER UN EVENEMENT-->
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
							<small>Creer un evenement</small>
							<a href="#" class="btn-block" data-toggle="modal" data-target="#bd-example-modal-lg" type="button">
								<img src="assets/images/modal-img1.jpg" alt="modal">
							</a>
							<div class="modal fade bs-example-modal-lg" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg modal-dialog-centered">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="myLargeModalLabel">Insérer une evenement</h4>
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										</div>
										<div class="modal-body">
											<form id="createForm">
												<div class="row">
													<!-- value = $_SESSION['id'] -->
													<input id='userId' type="text" class="d-none" value="5" name='id'>
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label>Début de l'evenement</label>
															<input type="date" class="form-control" name="start">
														</div>
													</div>
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label>Fin de l'évenement</label>
															<input type="date" class="form-control" name="end">
														</div>
													</div>
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label>Nom de l'evenement</label>
															<input type="text" class="form-control" name="title">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<!-- type event en fonction du role !!! -->
															<label>Type de l'evenement</label>
															<select id="typeEvent" type="text" class="form-control" name="type">
																<option value="rdv">rdv</option>
																<option value="autre">autre</option>
															</select>
														</div>
													</div>
													<div class="col-md-4 col-sm-12">
														<div class="row">
															<div class="form-group">
																<label>Ajouter des invités</label>
																<select type="text" class="form-control" id="select">
																	<?php foreach ($guests as $guest => $value) { ?>
																		<option id='<?php echo $value['id'] ?>' value="<?php echo $value['id'] ?>"><?php echo $value['FirstName'] ?></option>
																	<?php } ?>
																</select>
																<button type='button' id='addGuest'>Add</button>
															</div>
																	<div class="displayGuest ml-4 ">
		
																	</div>
															<input type="text" class="d-none" id='guests' name='guest'>
														</div>
													</div>
												</div>
										</div>
										<div class="col-md-8 col-sm-12">
											<textarea rows="5" cols="40" name='description'>
												</textarea>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
											<button id="createEvent" type="submit" class="btn btn-primary" name="createEvent">Créer l'evenement</button>
										</div>
										</form>
										<!-- <div id="ex1" class="modal">
											<p>Thanks for clicking. That felt good.</p>
											<a href="#" rel="modal:close">Close</a>
										</div> -->
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
				<!--Fenetre Pop-up pour MODIFIER UN EVENEMENT-->
				<!--Fenetre Pop-up pour MODIFIER UN EVENEMENT-->
				<!--Fenetre Pop-up pour MODIFIER UN EVENEMENT-->
				<!--Fenetre Pop-up pour MODIFIER UN EVENEMENT-->


				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
							<!-- ICI IL FAUT QUE J ARRIVE A VIRER LE BACK GROUND DU BOUTON-->
							<a href="#" class="btn-block" data-toggle="modal" data-target="#bd-example-modal-lg-test" type="hidden">

							</a>
							<div class="modal fade bs-example-modal-lg" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg modal-dialog-centered">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="EventTilteUpdate"></h4>
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										</div>
										<div class="modal-body">

											<form method="PUT">
												<div class="row">
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label>Début de l'evenement</label>
															<input id="startTimeEvent" type="date" class="datetimepicker form-control" name="start">

														</div>
													</div>
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label>Fin de l'évenement</label>
															<input id="endTimeEvent" type="date" class="datetimepicker form-control" name="end">
														</div>
													</div>
													<div class="col-md-4 col-sm-12">
														<div class="form-group">
															<label>Nom de l'evenement</label>
															<input id="nameEvent" type="text" class="form-control" name="title">
														</div>
													</div>
													
												</div>
												<!-- <div class="col-md-4 col-sm-12"> -->
														<div class="row col-sm-12">
															
																	<div class="displayGuestupdate ml-4 ">
		
																	</div>
																	
															<input type="text" class="d-none" id='guestsupdate' name='guest'>
														</div>
													<!-- </div> -->
												<textarea id="descriptionEvent" rows="5" cols="40">

												</textarea>

												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermeture</button>
													<button id="updateButton" class="btn btn-primary">Modifier</button>
													<button id="deleteButton" class="btn btn-danger">Supprimer</button>
													<!-- <button id="validateButton" class="btn btn-success">Valider l'évenement</button>
													<button id="refuseButton" class="btn btn-danger">Refuser l'évenement</button> -->



												</div>

										</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>


					<div class="pd-20 bg-white box-shadow mb-30">
						<div class="row calendar-wrap">
							<div class="col-xl-2 col-lg-3 col-md-12 col-sm-12">
								<!-- <div id='external-events'>
										<h4 class="mb-30">Draggable Events</h4>

										<div class='fc-event'>My Event 1</div>
										<div class='fc-event'>My Event 2</div>
										<div class='fc-event'>My Event 3</div>
										<div class='fc-event'>My Event 4</div>
										<div class='fc-event'>My Event 5</div>
										<div class="custom-control custom-checkbox mb-5">
											<input type='checkbox' class="custom-control-input" id='drop-remove' />
											<label class="custom-control-label" for='drop-remove'>remove after drop</label>
										</div>
									</div> -->
							</div>
							<div class="col-xl-10 col-lg-9 col-md-12 col-sm-12">
								<!-- DIV AFFICHAGE DU CALENDRIER -->
								<div id='calendar'></div>
							</div>
							<!-- FIN DIV AFFICHAGE DU CALENDRIER -->
						</div>
					</div>
				</div>

				<?php include('include/footer.php'); ?>
			</div>
		</div>


		<?php include('include/script.php'); ?>
		<script src="assets/plugins/fullcalendar/lib/jquery-ui.min.js"></script>
		<script src="assets/plugins/fullcalendar/fullcalendar.min.js"></script>
		<script src="assets/plugins/fullcalendar/lib/moment.min.js"></script>
		<script src="assets/scripts/calendar.js"></script>



</body>

</html>